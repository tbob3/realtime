<?php

use App\Events\UserSignedUp;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    // return phpinfo();
    // return Redis::get('name');
    // return view('welcome');

    $data = [
        'event' => 'UserSignedUp',
        'data' => [
            'username' => 'tohid',
        ],
    ];

    // Redis::publish('test-channel', json_encode($data));

    event(new UserSignedUp('tohid'));

    return view('welcome');

});
